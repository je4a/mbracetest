package com.getmbrace.flickrimagesearcher;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class FlickrImageAdapter extends BaseAdapter {
    private final List<String> list;
    private final Context context;

    public FlickrImageAdapter(Context context, List<String> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.flickr_image_layout, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.initItem((String) getItem(position));
        return convertView;
    }

    public void addAll(List<String> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        private ImageView photoView;

        public ViewHolder(View layout) {
            photoView = (ImageView) layout.findViewById(R.id.iv_flickr_photo);
        }

        public void initItem(String url) {
            Picasso.with(context)
                    .load(url)
                    .into(photoView);
        }
    }
}
