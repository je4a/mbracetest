package com.getmbrace.flickrimagesearcher;

import android.content.Context;
import android.content.SharedPreferences;

import com.googlecode.flickrjandroid.photos.Photo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class Util {
    private static final String photoUrlFormat = "http://farm%s.staticflickr.com/%s/%s_%s_%s.jpg";
    private static final String PREF_NAME = "flickr_searcher";
    private static final String PREF_HISTORY_ITEMS_KEY = "history_items_key";

    private static Util instance;
    private final Context context;
    private final SharedPreferences prefs;

    private Util(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static Util getInstance(Context context) {
        if (instance == null) {
            instance = new Util(context);
        }

        return instance;
    }

    private SharedPreferences.Editor getEditor() {
        return prefs.edit();
    }

    private Set<String> getTagsSet() {
        Set<String> tagsSet = prefs.getStringSet(PREF_HISTORY_ITEMS_KEY, null);

        if (tagsSet == null) {
            tagsSet = new HashSet<String>();
        }

        return tagsSet;
    }

    public void putTags(Set<String> tags) {
        getEditor()
            .putStringSet(PREF_HISTORY_ITEMS_KEY, tags)
            .apply();
    }

    public void putTagsWithLinks(String tags, Set<String> links) {
        getEditor()
            .putStringSet(tags, links)
            .apply();
    }

    public Map<String, Set<String>> getEntireHistory() {
        Map<String, Set<String>> history = new HashMap<String, Set<String>>();
        Set<String> tags = getTagsSet();

        for (String tag : tags) {
            if (prefs.contains(tag)) {
                history.put(tag, prefs.getStringSet(tag, null));
            }
        }

        return history;
    }

    public static String getImageUrl(Photo photo) {
        return String.format(photoUrlFormat, photo.getFarm(),
                photo.getServer(), photo.getId(), photo.getSecret(), "z"); // MEDIUM_640;
    }
}