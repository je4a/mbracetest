package com.getmbrace.flickrimagesearcher;

import android.os.AsyncTask;
import android.util.Log;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.SearchParameters;

import org.json.JSONException;

import java.io.IOException;

public class ImagesTask extends AsyncTask<Void, Void, Void> {
    private static final String LOG_TAG = ImagesTask.class.getSimpleName();
    private static final String FLICKR_API_KEY = "604a5697a109e042b00ab07f33d01e15";
    private static final int LIMIT = 15;

    private int offset;
    private final String tags;
    private ImageRequestFinishedListener imagesRequestFinishedListener;

    public ImagesTask(int offset, String tags, ImageRequestFinishedListener listener) {
        this.offset = offset;
        this.tags = tags;
        this.imagesRequestFinishedListener = listener;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Flickr flickr = new Flickr(FLICKR_API_KEY);
        SearchParameters searchParams = new SearchParameters();
        searchParams.setTags(tags.contains(" ") ? tags.split(" ") : new String[] { tags });

        try {
            PhotoList list = flickr.getPhotosInterface().search(searchParams, LIMIT, offset);

            if (list != null) {
                if (imagesRequestFinishedListener != null) {
                    imagesRequestFinishedListener.success(list);
                }
            }
        } catch (IOException e) {
            printError(e);
        } catch (FlickrException e) {
            printError(e);
        } catch (JSONException e) {
            printError(e);
        }

        return null;
    }

    private void printError(Exception e) {
        Log.e(LOG_TAG, e.getMessage(), e);

        if (imagesRequestFinishedListener != null) {
            imagesRequestFinishedListener.fail(e);
        }
    }
}
