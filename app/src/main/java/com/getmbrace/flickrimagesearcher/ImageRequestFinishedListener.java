package com.getmbrace.flickrimagesearcher;

import com.googlecode.flickrjandroid.photos.PhotoList;

public interface ImageRequestFinishedListener {
    public void success(PhotoList list);
    public void fail(Exception exception);
}
