package com.getmbrace.flickrimagesearcher;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SearcherActivity extends Activity {
    private static final String LOG_TAG = SearcherActivity.class.getName();

    private ListView photosListView;
    private EditText tagsEditText;
    private FlickrImageAdapter imageAdapter;
    private Map<String, Set<String>> searchHistory = new HashMap<String, Set<String>>();
    private List<String> historyTags = new ArrayList<String>();
    private Spinner historyItemsSpinner;
    private ArrayAdapter<String> spinnerAdapter;
    private Util util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searcher);

        util = Util.getInstance(getApplicationContext());
        photosListView = (ListView) findViewById(android.R.id.list);
        tagsEditText = (EditText) findViewById(R.id.et_query_tags);
        historyItemsSpinner = (Spinner) findViewById(R.id.sp_history);

        findViewById(R.id.btn_search).setOnClickListener(getSearchBtnClickListener());

        imageAdapter = getFlickrImageAdapter(new ArrayList<String>());
        spinnerAdapter = getSpinnerAdapter(new ArrayList<String>());
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        historyItemsSpinner.setAdapter(spinnerAdapter);
        historyItemsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!historyTags.get(position).equals(tagsEditText.getText().toString())) {
                    tagsEditText.setText(historyTags.get(position));
                    if (isNetworkAvailable()) {
                        checkTextAndStartDownloadTask();
                    } else {
                        photosListView.setAdapter(
                                getFlickrImageAdapter(new ArrayList<String>(searchHistory
                                        .get(historyTags.get(position)))));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                if (historyTags.size() > 0) {
                    tagsEditText.setText(historyTags.get(0));
                }
            }
        });
    }

    private FlickrImageAdapter getFlickrImageAdapter(List<String> data) {
        return new FlickrImageAdapter(getApplicationContext(), data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        searchHistory = util.getEntireHistory();
        if (searchHistory.size() > 0) {
            Log.i(LOG_TAG, "network is not available so trying to get cache");

            historyTags.addAll(searchHistory.keySet());
            spinnerAdapter.addAll(historyTags);
            spinnerAdapter.notifyDataSetChanged();

            Set<String> urls = searchHistory.get(historyTags.get(0));
            if (urls != null) {
                List<String> urlsList = new ArrayList<String>(urls);
                imageAdapter.addAll(urlsList);
            }
        }
    }

    private ArrayAdapter<String> getSpinnerAdapter(List<String> data) {
        return new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, data);
    }

    private void startImagesTask(final String tags) {
        showProgress();

        //first page only
        new ImagesTask(0, tags, new ImageRequestFinishedListener() {
            @Override
            public void success(PhotoList list) {
                final List<String> urls = new ArrayList<String>(list.size());
                for (Photo photo : list) {
                    urls.add(Util.getImageUrl(photo));
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        photosListView.setAdapter(getFlickrImageAdapter(urls));

                        if (!historyTags.contains(tags)) {
                            historyTags.add(tags);
                            util.putTags(new HashSet<String>(historyTags));
                            spinnerAdapter.add(tags);
                            spinnerAdapter.notifyDataSetChanged();
                            historyItemsSpinner.setSelection(historyTags.indexOf(tags), false);
                        }
                        hideProgress();
                    }
                });

                searchHistory.get(tags).addAll(urls);
                util.putTagsWithLinks(tags, new HashSet<String>(urls));
            }

            @Override
            public void fail(Exception exception) {
                hideProgress();
            }
        }).execute();
    }

    private View.OnClickListener getSearchBtnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkTextAndStartDownloadTask();
                try {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(SearcherActivity.this.getWindow().getDecorView().getWindowToken(), 0);
                } catch (Exception ex) {
                    // don't care
                }
            }
        };
    }

    private void checkTextAndStartDownloadTask() {
        if (tagsEditText.getText().length() > 0) {
            String text = tagsEditText.getText().toString().trim();

            if (!text.isEmpty()) {
                if (!searchHistory.containsKey(text)) {
                    searchHistory.put(text, new HashSet<String>());
                }

                startImagesTask(text);
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }

        return false;
    }

    private void showOrHideProgress(final boolean visibility) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(visibility);
            }
        });
    }

    private void showProgress() {
        showOrHideProgress(true);
    }

    private void hideProgress() {
        showOrHideProgress(false);
    }
}